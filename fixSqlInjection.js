(async function() {
  'use strict';

  // Lower is more verbose.
  const LOG_LEVEL = 2;

  const fs                 = require('fs');
  const glob               = require('glob');
  const prompt             = require('prompt');
  const diff               = require('diff');
  const colors             = require('colors');
  const promisify          = require('./promisify');
  const Logger             = require('./Logger');
  const logger             = new Logger(LOG_LEVEL);
  const QueryIterator      = require('./QueryIterator');
  const VulnerabilityFixer = require('./VulnerabilityFixer');
  const find               = promisify(glob);
  const readFile           = promisify(fs.readFile);
  const writeFile          = promisify(fs.writeFile);
  const promptGet          = promisify(prompt.get);
  const globOpts           = {
    absolute: true
  };

  logger.clearTerm();

  const argv = require('yargs')
    .usage('Uage: $0 [<ColdFusion project directory>')
    .demandCommand(1, 1)
    .argv;

  globOpts.cwd      = argv._[0];
  globOpts.absolute = true;

  // Find all CF files.
  const files = await find('**/*.{cvn,cfc,cfm}', globOpts);

  // Log of files that have been fixed, which allows the user to skip over
  // files that have already been analyzed.
  let fixedFiles = [];

  if (fs.existsSync('./fixedFiles.json')) {
    fixedFiles = await readFile('./fixedFiles.json', 'utf8');
    fixedFiles = JSON.parse(fixedFiles);
  }

  // Iterate over each file and read the contents.
  for (let file of files) {
    if (fixedFiles.indexOf(file) !== -1)
      continue;

    // Keep track of files that have been analyzed.
    fixedFiles.push(file);
    await writeFile('./fixedFiles.json', JSON.stringify(fixedFiles, null, 2), 'utf8');

    const content       = await readFile(file, 'utf8');
    const queryIterator = new QueryIterator(content, file, logger);

    // Iterate over each query in the file.  Each query object has
    // fileIndices, sql, and file properties.
    for (let query of queryIterator) {
      const fixedSql = await processQuery(query.sql);

      // If the query was changed (fixed), write it to disk.
      if (fixedSql !== query.sql) {
        logger.log(`Writing query to file ${file}`, 2);

        await queryIterator.writeQuery(fixedSql, query.fileIndices);
      }

      logger.clearTerm();
    }
  }

  // Find all vulnerable variables in a query and parameterize them.
  async function processQuery(sql) {
    logger.log(`Processing query:`, 2);
    logger.log(sql, 2);

    const vulnFixer = new VulnerabilityFixer(sql, logger);

    vulnFixer.fixVulnerabilities();

    if (vulnFixer.vulnCount) {
      logger.log(`\nVulnerable query contains ${vulnFixer.vulnCount} vulnerabilities.  Query:`, 3);
      logger.log(sql, 3);
      logger.log('\nFixed query:', 3);
      logger.log(vulnFixer.fixedSql, 3);

      logger.log('Diff:\n', 3);

      diff
        .diffChars(sql, vulnFixer.fixedSql)
        .forEach(part => {
          let color;

          if (part.added)
            color = 'cyan';
          else if (part.removed)
            color = 'red';
          else
            color = 'magenta';

          logger.log(part.value[color], 3, false);
        });
      logger.log('', 3);

      const answer = await promptForFix();

      return answer.fix === 'y' ? vulnFixer.fixedSql : sql;
    }

    return sql;
  }

  // Ask the user if they want to use the fixed version of the query.
  function promptForFix() {
    const pSchema = {
      properties: {
        fix: {
          pattern : /[yn]/,
          message : 'Use fixed version of query? (y/n): ',
          required: true
        }
      }
    };

    return promptGet(pSchema);
  }
})();

