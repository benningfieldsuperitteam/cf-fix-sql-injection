'use strict';

const EOL       = require('os').EOL;
const fs        = require('fs');
const promisify = require('./promisify');
const writeFile = promisify(fs.writeFile);

class QueryIterator {
  /**
   * Initialize the iterator using the content of a file.
   */
  constructor(content, file, logger) {
    this.content = content;
    this.file    = file;
    this.logger  = logger;
  }

  /**
   * Iterate over the queries in the file.
   */
  *[Symbol.iterator]() {
    const lines = this.content
      .split(EOL);

    const queryIndices = lines
      .reduce((queryIndices, line, lineInd) => {
        // Pairs of line indices where for <cfquery> tags (not including the
        // tags, just the SQL).
        if (line.match(/<cfquery /i))
          queryIndices.push([lineInd + 1]);
        else if (line.match(/<\/cfquery>/i))
          queryIndices[queryIndices.length - 1].push(lineInd);

        return queryIndices;
      }, []);

    for (let fileIndices of queryIndices) {
      const sql = lines
        .slice(...fileIndices)
        .join(EOL);

      this.logger.log(`Found a query in file ${this.file}:${fileIndices[0]+1}-${fileIndices[1]}`, 2);

      yield {fileIndices, sql, file: this.file};
    }
  }

  /**
   * Write sql to a file and indices.
   */
  writeQuery(sql, indices) {
    const lines = this.content
      .split(EOL);

    lines
      .splice(indices[0], indices[1] - indices[0], ...sql
        .split(EOL));

    this.content = lines
      .join(EOL);

    this.logger.log('Fixed content:', 1);
    this.logger.log(this.content, 1);

    return writeFile(this.file, this.content, 'utf8');
  }
}

module.exports = QueryIterator;

