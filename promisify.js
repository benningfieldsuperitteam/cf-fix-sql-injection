'use strict';

module.exports = promisify;

function promisify(cbFunc) {
  return function(...args) {
    return new Promise(function(resolve, reject) {
      cbFunc(...args, function(err, data) {
        if (err)
          reject(err);
        else
          resolve(data);
      });
    });
  }
}

