'use strict';

const clear = require('clear');

class Logger {
  /**
   * Initialize the logger, which logs anything >= logLevel.
   */
  constructor(logLevel) {
    this.logLevel = logLevel;
  }

  /**
   * Log with a level.
   */
  log(msg, lvl, addNewline = true) {
    if (lvl >= this.logLevel) {
      if (addNewline)
        console.log(msg);
      else
        process.stdout.write(msg);
    }
  }

  /**
   * Clear the terminal.
   */
  clearTerm() {
    clear();
  }
}

module.exports = Logger;

