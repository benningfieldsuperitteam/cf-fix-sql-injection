'use strict';

const EOL = require('os').EOL;

class QueryParser {
  /**
   * Init.
   */
  constructor() {
  }

  /**
   * Parse a query into a series of tokens.
   */
  parseQuery(query) {
    const tokens = [];

    while (query.length) {
      let token = null;

      // IN is a special operator, since it looks like a word.
      if (query.match(/^IN\b/)) {
        token = {
          type: 'OP',
          value: 'IN'
        };
      }
      // Variables are special, since they aren't standard SQL.
      else if (query[0] === '#') {
        const EOLRE = new RegExp(EOL, 'g');

        token = {
          type: 'VAR',
          value: query
            .replace(EOLRE, '')
            .replace(/^#([^#]+)#.*$/, '#$1#')
        };
      }
      // Query parameters are special since they can be ignored.
      else if (query.match(/^<cfqueryparam/i)) {
        token = {
          type: 'PARAM',
          value: ''
        };

        // A param can cross multiple lines, so move forward until the end is
        // reached.
        for (let i = 0; i < query.length; ++i) {
          token.value += query[i];

          if (query[i] === '>')
            break;
        }
      }
      else {
        // Parse the next token using a stack.
        const chars = query
          .split('')
          .reverse();
        const ch          = chars.pop();
        const opRE        = /[<>!=%]/;
        const wordRE      = /[A-Za-z0-9\._]/;
        const getCharType = ch => {
          if (ch.match(wordRE))
            return 'WORD';
          else if (ch.match(opRE))
            return 'OP';
          else if (ch === '\'')
            return 'QUOTE';
          else
            return 'OTHER';
        };

        token = {
          type  : getCharType(ch),
          value : ch
        }

        if (token.type === 'WORD') {
          while (chars.length) {
            const ch       = chars[chars.length - 1];
            const charType = getCharType(ch);

            if (charType !== token.type)
              break;

            token.value += chars.pop();
          }
        }
      }

      tokens.push(token);

      // Remove the token from the query and move on.
      query = query.replace(token.value, '');
    }

    return tokens;
  }
}

module.exports = QueryParser;

